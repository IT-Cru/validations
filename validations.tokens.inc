<?php

/**
 * @file
 * Builds placeholder replacement tokens for validation-related data.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\user\Entity\User;

/**
 * Implements hook_token_info().
 */
function validations_token_info() {
  $type = [
    'name' => t('Validations'),
    'description' => t('Tokens related to validations.'),
    'needs-data' => 'validation_entity',
  ];

  // Core tokens for validations.
  $validation['id'] = [
    'name' => t("Validation ID"),
    'description' => t('The unique ID of validation.'),
  ];
  $validation['vid'] = [
    'name' => t("Revision ID"),
    'description' => t("The unique ID of the validation's latest revision."),
  ];
  $validation['type'] = [
    'name' => t("Validation type"),
  ];
  $validation['title'] = [
    'name' => t("Title"),
  ];
  $validation['body'] = [
    'name' => t("Body"),
    'description' => t("The main body text of the validation."),
  ];
  $validation['summary'] = [
    'name' => t("Summary"),
    'description' => t("The summary of the validation's main body text."),
  ];
  $validation['langcode'] = [
    'name' => t('Language code'),
    'description' => t('The language code of the language the validation is written in.'),
  ];
  $validation['url'] = [
    'name' => t("URL"),
    'description' => t("The URL of the validation."),
  ];
  $validation['edit-url'] = [
    'name' => t("Edit URL"),
    'description' => t("The URL of the validation's edit page."),
  ];

  // Chained tokens for validations.
  $validation['created'] = [
    'name' => t("Date created"),
    'type' => 'date',
  ];
  $validation['changed'] = [
    'name' => t("Date changed"),
    'description' => t("The date the validation was most recently updated."),
    'type' => 'date',
  ];
  $validation['author'] = [
    'name' => t("Author"),
    'type' => 'user',
  ];

  return [
    'types' => ['validation_entity' => $type],
    'tokens' => ['validation_entity' => $validation],
  ];
}

/**
 * Implements hook_tokens().
 */
function validations_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();

  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = LanguageInterface::LANGCODE_DEFAULT;
  }
  $replacements = [];

  if ($type == 'validation_entity' && !empty($data['validation_entity'])) {
    /** @var \Drupal\validations\Entity\ValidationEntityInterface $validation */
    $validation = $data['validation_entity'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the validation.
        case 'id':
          $replacements[$original] = $validation->id();
          break;

        case 'vid':
          $replacements[$original] = $validation->getRevisionId();
          break;

        case 'type':
          $replacements[$original] = $validation->bundle();
          break;

        case 'title':
          $replacements[$original] = $validation->label();
          break;

        case 'langcode':
          $replacements[$original] = $validation->language()->getId();
          break;

        case 'url':
          $replacements[$original] = $validation->url('canonical', $url_options);
          break;

        case 'edit-url':
          $replacements[$original] = $validation->url('edit-form', $url_options);
          break;

        // Default values for the chained tokens handled below.
        case 'author':
          $account = $validation->getOwner() ? $validation->getOwner() : User::load(0);
          $bubbleable_metadata->addCacheableDependency($account);
          $replacements[$original] = $account->label();
          break;

        case 'created':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = format_date($validation->getCreatedTime(), 'medium', '', NULL, $langcode);
          break;

        case 'changed':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = format_date($validation->getChangedTime(), 'medium', '', NULL, $langcode);
          break;
      }
    }

    if ($author_tokens = $token_service->findWithPrefix($tokens, 'author')) {
      $replacements += $token_service->generate('user', $author_tokens, ['user' => $validation->getOwner()], $options, $bubbleable_metadata);
    }

    if ($created_tokens = $token_service->findWithPrefix($tokens, 'created')) {
      $replacements += $token_service->generate('date', $created_tokens, ['date' => $validation->getCreatedTime()], $options, $bubbleable_metadata);
    }

    if ($changed_tokens = $token_service->findWithPrefix($tokens, 'changed')) {
      $replacements += $token_service->generate('date', $changed_tokens, ['date' => $validation->getChangedTime()], $options, $bubbleable_metadata);
    }
  }

  return $replacements;
}
