<?php

namespace Drupal\gvalidations\Routing;

use Drupal\validations\Entity\ValidationEntityType;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for group_validation group content.
 */
class GroupValidationsRouteProvider {

  /**
   * Provides the shared collection route for group validation plugins.
   */
  public function getRoutes() {
    $routes = $plugin_ids = $permissions_add = $permissions_create = [];

    foreach (ValidationEntityType::loadMultiple() as $name => $validation_entity_type) {
      $plugin_id = "group_validation:$name";

      $plugin_ids[] = $plugin_id;
      $permissions_add[] = "create $plugin_id content";
      $permissions_create[] = "create $plugin_id entity";
    }

    // If there are no validation types yet, we cannot have any plugin IDs and
    // should therefore exit early because we cannot have any routes for them.
    if (empty($plugin_ids)) {
      return $routes;
    }

    $routes['entity.group_content.group_validation_relate_page'] = new Route('group/{group}/validation/add');
    $routes['entity.group_content.group_validation_relate_page']
      ->setDefaults([
        '_title' => 'Relate validation',
        '_controller' => '\Drupal\gvalidations\Controller\GroupValidationsController::addPage',
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_add))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    $routes['entity.group_content.group_validation_add_page'] = new Route('group/{group}/validation/create');
    $routes['entity.group_content.group_validation_add_page']
      ->setDefaults([
        '_title' => 'Create validation',
        '_controller' => '\Drupal\gvalidations\Controller\GroupValidationsController::addPage',
        'create_mode' => TRUE,
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_create))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    return $routes;
  }

}
