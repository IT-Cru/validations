<?php

namespace Drupal\gvalidations\Plugin\GroupContentEnabler;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\validations\Entity\ValidationEntityType;
use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Class GroupValidationsDeriver.
 *
 * @package Drupal\gvalidations\Plugin\GroupContentEnabler
 */
class GroupValidationsDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (ValidationEntityType::loadMultiple() as $name => $validation_entity_type) {
      $label = $validation_entity_type->label();

      $this->derivatives[$name] = [
        'entity_bundle' => $name,
        'label' => $this->t('Group validation (@type)', ['@type' => $label]),
        'description' => $this->t('Adds %type content to groups both publicly and privately.', ['%type' => $label]),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
