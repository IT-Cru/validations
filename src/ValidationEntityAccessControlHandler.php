<?php

namespace Drupal\validations;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Validation entity.
 *
 * @see \Drupal\validations\Entity\ValidationEntity.
 */
class ValidationEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\validations\Entity\ValidationEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished validation entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published validation entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit validation entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete validation entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add validation entities');
  }

}
