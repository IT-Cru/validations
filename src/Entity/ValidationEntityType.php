<?php

namespace Drupal\validations\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Validation type entity.
 *
 * @ConfigEntityType(
 *   id = "validation_entity_type",
 *   label = @Translation("Validation type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\validations\ValidationEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\validations\Form\ValidationEntityTypeForm",
 *       "edit" = "Drupal\validations\Form\ValidationEntityTypeForm",
 *       "delete" = "Drupal\validations\Form\ValidationEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\validations\ValidationEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "validation_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "validation_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/validation_entity_type/{validation_entity_type}",
 *     "add-form" = "/admin/structure/validation_entity_type/add",
 *     "edit-form" = "/admin/structure/validation_entity_type/{validation_entity_type}/edit",
 *     "delete-form" = "/admin/structure/validation_entity_type/{validation_entity_type}/delete",
 *     "collection" = "/admin/structure/validation_entity_type"
 *   }
 * )
 */
class ValidationEntityType extends ConfigEntityBundleBase implements ValidationEntityTypeInterface {

  /**
   * The Validation type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Validation type label.
   *
   * @var string
   */
  protected $label;

}
