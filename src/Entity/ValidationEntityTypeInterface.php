<?php

namespace Drupal\validations\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Validation type entities.
 */
interface ValidationEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
