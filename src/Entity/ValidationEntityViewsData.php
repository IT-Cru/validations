<?php

namespace Drupal\validations\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Validation entities.
 */
class ValidationEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
