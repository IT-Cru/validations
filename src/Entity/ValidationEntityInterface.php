<?php

namespace Drupal\validations\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Validation entities.
 *
 * @ingroup validations
 */
interface ValidationEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Validation name.
   *
   * @return string
   *   Name of the Validation.
   */
  public function getName();

  /**
   * Sets the Validation name.
   *
   * @param string $name
   *   The Validation name.
   *
   * @return \Drupal\validations\Entity\ValidationEntityInterface
   *   The called Validation entity.
   */
  public function setName($name);

  /**
   * Gets the Validation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Validation.
   */
  public function getCreatedTime();

  /**
   * Sets the Validation creation timestamp.
   *
   * @param int $timestamp
   *   The Validation creation timestamp.
   *
   * @return \Drupal\validations\Entity\ValidationEntityInterface
   *   The called Validation entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Validation published status indicator.
   *
   * Unpublished Validation are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Validation is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Validation.
   *
   * @param bool $published
   *   TRUE to set this Validation to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\validations\Entity\ValidationEntityInterface
   *   The called Validation entity.
   */
  public function setPublished($published);

  /**
   * Returns the Validation valid status indicator.
   *
   * @return bool
   *   TRUE if the Validation is valid.
   */
  public function isValid();

  /**
   * Sets the valid status of a Validation.
   *
   * @param bool $valid
   *   TRUE to set this Validation to valid, FALSE to set it to invalid.
   *
   * @return \Drupal\validations\Entity\ValidationEntityInterface
   *   The called Validation entity.
   */
  public function setValid($valid);

  /**
   * Gets the Validation revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Validation revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\validations\Entity\ValidationEntityInterface
   *   The called Validation entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Validation revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Validation revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\validations\Entity\ValidationEntityInterface
   *   The called Validation entity.
   */
  public function setRevisionUserId($uid);

}
