<?php

namespace Drupal\validations\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Validation entities.
 *
 * @ingroup validations
 */
class ValidationEntityDeleteForm extends ContentEntityDeleteForm {


}
