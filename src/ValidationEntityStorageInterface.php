<?php

namespace Drupal\validations;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\validations\Entity\ValidationEntityInterface;

/**
 * Defines the storage handler class for Validation entities.
 *
 * This extends the base storage class, adding required special handling for
 * Validation entities.
 *
 * @ingroup validations
 */
interface ValidationEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Validation revision IDs for a specific Validation.
   *
   * @param \Drupal\validations\Entity\ValidationEntityInterface $entity
   *   The Validation entity.
   *
   * @return int[]
   *   Validation revision IDs (in ascending order).
   */
  public function revisionIds(ValidationEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Validation author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Validation revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\validations\Entity\ValidationEntityInterface $entity
   *   The Validation entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ValidationEntityInterface $entity);

  /**
   * Unsets the language for all Validation with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
