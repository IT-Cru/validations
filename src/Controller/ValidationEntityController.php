<?php

namespace Drupal\validations\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\validations\Entity\ValidationEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ValidationEntityController.
 *
 *  Returns responses for Validation routes.
 */
class ValidationEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a ValidationEntityController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(DateFormatterInterface $date_formatter, RendererInterface $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Validation revision.
   *
   * @param int $validation_entity_revision
   *   The Validation  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($validation_entity_revision) {
    $validation_entity = $this->entityManager()->getStorage('validation_entity')->loadRevision($validation_entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder('validation_entity');

    return $view_builder->view($validation_entity);
  }

  /**
   * Page title callback for a Validation  revision.
   *
   * @param int $validation_entity_revision
   *   The Validation  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($validation_entity_revision) {
    $validation_entity = $this->entityManager()->getStorage('validation_entity')->loadRevision($validation_entity_revision);
    $date = $this->dateFormatter->format($validation_entity->getRevisionCreationTime(), 'short');
    return $this->t('Revision of %title from %date', ['%title' => $validation_entity->label(), '%date' => $date]);
  }

  /**
   * Generates an overview table of older revisions of a Validation.
   *
   * @param \Drupal\validations\Entity\ValidationEntityInterface $validation_entity
   *   A Validation  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ValidationEntityInterface $validation_entity) {
    $account = $this->currentUser();
    $langcode = $validation_entity->language()->getId();
    $langname = $validation_entity->language()->getName();
    $languages = $validation_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $validation_entity_storage = $this->entityManager()->getStorage('validation_entity');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $validation_entity->label()]) : $this->t('Revisions for %title', ['%title' => $validation_entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all validation revisions") || $account->hasPermission('administer validation entities')));
    $delete_permission = (($account->hasPermission("delete all validation revisions") || $account->hasPermission('administer validation entities')));

    $rows = [];

    $vids = $validation_entity_storage->revisionIds($validation_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\validations\ValidationEntityInterface $revision */
      $revision = $validation_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $validation_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.validation_entity.revision', ['validation_entity' => $validation_entity->id(), 'validation_entity_revision' => $vid]));
        }
        else {
          $link = $validation_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.validation_entity.revision_revert', ['validation_entity' => $validation_entity->id(), 'validation_entity_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.validation_entity.revision_delete', ['validation_entity' => $validation_entity->id(), 'validation_entity_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['validation_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
